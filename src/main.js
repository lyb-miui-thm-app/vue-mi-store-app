import Vue from 'vue'
import App from './App.vue'
import LazyLoad from './utils/lazyLoad'

Vue.config.productionTip = false
Vue.use(LazyLoad);

new Vue({
  render: h => h(App),
}).$mount('#app')
